Gem::Specification.new do |s|
  s.name        = 'html_to_image'
  s.version     = '1.0.0'
  s.date        = '2017-11-13'
  s.summary     = 'Wrapper around wkhtmltopdf and wkhtmltojpeg'
  s.description = 'Run wkhtmltopdf/ wkhtmltojpeg to convert html file to pdf or jpeg'
  s.authors     = ["Viktor Penkov"]
  s.email       = 'vpenkoff@gmail.com'
  s.files       = Dir['lib/*.rb', 'lib/html_to_image/*.rb', 'lib/html_to_image/bin/*']
  s.homepage    = 'https://github.com/vpenkoff/html_to_image'
  s.license     = 'MIT'
end
