require_relative 'html_to_image/utils.rb'
require_relative 'html_to_image/cmd.rb'
require_relative 'html_to_image/pdf.rb'
require_relative 'html_to_image/jpeg.rb'

module HtmlToImage
  module_function

  def convert(format, src_data)
    Utils.validate_convert_args(format, src_data)
    Module.class_eval(format.to_s.upcase).public_send(:convert, src_data)
  end
end
