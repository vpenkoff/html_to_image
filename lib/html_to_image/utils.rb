module HtmlToImage
  module Utils
    module_function

    def validate_convert_args(format, src_data)
      validate_format(format)
      validate_src_data(src_data)
    end

    def validate_format(format)
      raise StandardError, "#{format} not supported! Please provide either :pdf or :jpeg" unless %i[pdf jpeg].include? format
    end

    def validate_src_data(input_data)
      raise StandardError, 'Please provide input data!' if input_data.nil? || input_data == ''
      raise StandardError, 'The given data is not a string!' unless input_data.respond_to? :to_s
    end

    def check_binary(path_to_binary)
      raise StandardError, "Cannot find #{path_to_binary}" unless binary_exists? path_to_binary
      raise StandardError, "Cannot execute #{path_to_binary}" unless binary_executable? path_to_binary
    end

    def binary_exists?(path_to_binary)
      File.exist?(path_to_binary)
    end

    def binary_executable?(path_to_binary)
      File.executable?(path_to_binary)
    end

    def expand_cmd(cmd)
      root_path = File.dirname(Gem.find_files('html_to_image').first)
      raise "#{root_path} is not a directory!" unless File.directory?(root_path)
      cmd_path = File.join(root_path, 'html_to_image', 'bin', cmd)
      check_binary(cmd_path)
      cmd_path
    end
  end
end
