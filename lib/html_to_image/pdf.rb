module HtmlToImage
  module PDF
    CMD = 'wkhtmltopdf'.freeze
    CMD_OPTS = '--disable-smart-shrinking'.freeze

    module_function

    def convert(input)
      expanded_cmd = Utils.expand_cmd(CMD)
      Cmd.exec(input, expanded_cmd)
    end
  end
end
