module HtmlToImage
  module JPEG
    CMD = 'wkhtmltoimage'

    module_function

    def convert(input)
      expanded_cmd = Utils.expand_cmd(CMD)
      Cmd.exec(input, expanded_cmd)
    end
  end
end
