require 'open3'
module HtmlToImage
  module Cmd
    module_function

    def exec(input = '', *args)
      stdin, stdout, stderr = Open3.popen3("#{args.join(' ')} - -")
      stdin.puts(input) if input
      stdin.close
      stdout.readlines.join
    ensure
      [stdin, stdout, stderr].each { |io| io.close unless io.nil? || io.closed? }
    end
  end
end
